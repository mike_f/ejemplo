# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# -------------------------------------------------------------------------
# This is a sample controller
# - index is the default action of any application
# - user is required for authentication and authorization
# - download is for downloading files uploaded in the db (does streaming)
# -------------------------------------------------------------------------
import json

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    # response.flash = T("día 2")
    # persona = db().select(db.persona.id, db.persona.nombre. db.persona.direccion, db.persona.sexo)
    return dict(persona = "hola")


def proveedor():

    return dict()

def leer_persona():
    # persona = db().select(db.persona.id, db.persona.nombre, db.persona.direccion, db.persona.sexo).as_dict()
    persona = db().select(db.persona.ALL).as_dict()
    return json.dumps(persona)

def guardar_persona():
    resultado = {'exito' : False}
    id_form = request.post_vars
    print id_form
    if id_form.id:
        db(db.persona.id == id_form.id).update(nombre = id_form.nombre, direccion = id_form.direccion, sexo = id_form.sexo)
        resultado['exito'] = True
        resultado['id'] = id_form.id
        resultado['nombre'] = request.post_vars.nombre
        resultado['direccion'] = request.post_vars.direccion
        resultado['sexo'] = request.post_vars.sexo
    else:
        # id = db.persona.insert(**request.post_vars)
        id = db.persona.insert(nombre = id_form.nombre, direccion = id_form.direccion, sexo = id_form.sexo, es_cliente = "0", es_proveedor = "0")
        resultado['exito'] = True
        resultado['id'] = id
        resultado['nombre'] = request.post_vars.nombre
        resultado['direccion'] = request.post_vars.direccion
        resultado['sexo'] = request.post_vars.sexo

    return json.dumps(resultado)       
         

def eliminar_persona():
    id_persona = request.post_vars.id
    del db.persona[id_persona]
    # return json.dumps(resultado)

def modificar_persona():
    id_persona = request.post_vars.id
    data = db(db.persona.id == id_persona).select().first().as_dict()
    return json.dumps(data)

def cliente():
    return dict()

def leer_cliente():
    cliente = db(db.persona.es_cliente == 1).select(db.cliente.ALL).as_dict()
    return json.dumps(cliente)

def guardar_cliente():
    resultado = {'exito' : False}
    form = request.post_vars
    if form.id:
        db(db.cliente.id == form.id).update(estatus = form.estatus, limite_credito = form.limite_credito, descuento = form.descuento)
        resultado['exito'] = True
        resultado['id'] = form.id
        resultado['estatus'] = request.post_vars.estatus
        resultado['limite_credito'] = request.post_vars.limite_credito
        resultado['descuento'] = request.post_vars.descuento
    else:
        id = db.cliente.insert(persona_id = form.slt_personas, estatus = form.estatus, limite_credito = form.limite_credito, descuento = form.descuento)
        resultado['exito'] = True
        resultado['id'] = id
        resultado['persona_id'] = request.post_vars.slt_personas
        resultado['estatus'] = request.post_vars.estatus
        resultado['limite_credito'] = request.post_vars.limite_credito
        resultado['descuento'] = request.post_vars.descuento
        db(db.persona.id == form.slt_personas).update(es_cliente = "1")

    return json.dumps(resultado)       
         


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()

def buscar_persona():
    data = db(db.persona.id>0).select(db.persona.id, db.persona.nombre).as_dict()
    return json.dumps( data )

def buscar_persona_nocliente():
    data = db(db.persona.es_cliente=="0").select(db.persona.id, db.persona.nombre).as_dict()
    return json.dumps( data )

def buscar_persona_noproveedor():
    data = db(db.persona.es_proveedor=="0").select(db.persona.id, db.persona.nombre).as_dict()
    return json.dumps( data )


def guardar_proveedor():
    resultado = {'exito' : False}
    id_form = request.post_vars

    print id_form
    if id_form.id:
        db(db.proveedor.id == id_form.id).update(persona_id = id_form.persona_id, estatus = id_form.estatus, 
            dias_credito = id_form.dias_credito, condicion_pago = id_form.condicion_pago)
        resultado['exito'] = True
        resultado['id'] = id_form.id
        resultado['persona_id'] = request.post_vars.persona_id
        resultado['estatus'] = request.post_vars.estatus
        resultado['dias_credito'] = request.post_vars.dias_credito
        resultado['condicion_pago'] = request.post_vars.condicion_pago
    else:
        # id = db.persona.insert(**request.post_vars)
        id = db.proveedor.insert(persona_id = id_form.persona_id, estatus = id_form.estatus, dias_credito = id_form.dias_credito, 
            condicion_pago = id_form.condicion_pago)
        resultado['exito'] = True
        resultado['id'] = id
        resultado['persona_id'] = request.post_vars.persona_id
        resultado['estatus'] = request.post_vars.estatus
        resultado['dias_credito'] = request.post_vars.dias_credito
        resultado['condicion_pago'] = request.post_vars.condicion_pago
        db(db.persona.id == id_form.persona_id).update(es_proveedor = "1")

    return json.dumps(resultado)    


def leer_proveedor():
    # persona = db().select(db.persona.id, db.persona.nombre, db.persona.direccion, db.persona.sexo).as_dict()
    proveedor = db(db.persona.es_proveedor == "1").select(db.proveedor.ALL).as_dict()
    return json.dumps(proveedor)


def eliminar_proveedor():
    id_proveedor = request.post_vars.id
    persona_id = db(db.proveedor.id==id_proveedor).select(db.proveedor.persona_id).first().persona_id
    del db.proveedor[id_proveedor]

    id_form = request.post_vars
    db(db.persona.id == persona_id).update(es_proveedor = "0")
    resultado = {'exito' : False}



def modificar_proveedor():
    id_proveedor = request.post_vars.id
    data = db(db.proveedor.id == id_proveedor).select().first().as_dict()
    return json.dumps(data)
